package com.example.tp2;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;


public class VinActivity extends AppCompatActivity {
    public Wine wine = null;
    public static final String ADD = "add";
    public static final String UPDATE = "update";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wine = getIntent().getParcelableExtra("wine");
        setContentView(R.layout.activity_wine);
        final String type = getIntent().getStringExtra("type");
        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        final EditText name = findViewById(R.id.wineName);
        final EditText region = findViewById(R.id.editWineRegion);
       final EditText loc = findViewById(R.id.editLoc);
        final EditText plantedArea = findViewById(R.id.editPlantedArea);
        final EditText climate = findViewById(R.id.editClimate);
        climate.setText(wine.getClimate());
        plantedArea.setText(wine.getPlantedArea());
        loc.setText(wine.getLocalization());
        region.setText(wine.getRegion());
        name.setText(wine.getTitle());
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wine.setClimate(climate.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setPlantedArea(plantedArea.getText().toString());
                    wine.setLocalization(loc.getText().toString());
                    wine.setTitle(name.getText().toString());
                    boolean res = false;
                    if (type.equals(UPDATE))
                        res = wineDbHelper.updateWine(wine);
                    else if (type.equals(ADD)) {
                        System.out.println("Here");
                        res = wineDbHelper.addWine(null, wine);
                    }
                    if (res) {
                        Snackbar.make(v, "Vin Enregistré !", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    } else {
                        Snackbar.make(v, "Erreur !", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
                catch (DuplicateWineException e){
                    Snackbar.make(v, "Erreur le Vin existe deja", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                catch (EmptyWineException e){
                    Snackbar.make(v, "Le nom du vin ne doit pas etre vide", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

    }
}
