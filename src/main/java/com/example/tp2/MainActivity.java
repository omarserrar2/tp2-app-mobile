package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity implements View.OnCreateContextMenuListener {
    WineDbHelper wineDB;
    ListView listeVin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listeVin = findViewById(R.id.vin_liste);
         wineDB = new WineDbHelper(this);
        Cursor c = wineDB.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter (this, android.R.layout.simple_list_item_2, c, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2});
       listeVin.setAdapter(adapter);
       listeVin.setOnItemClickListener(new AdapterView.OnItemClickListener(){

             public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                 Intent myIntent = new Intent(MainActivity.this, VinActivity.class);
                 Wine wine = wineDB.findWineById(id);
                 myIntent.putExtra("wine", wine);
                 myIntent.putExtra("type", VinActivity.UPDATE);
                 MainActivity.this.startActivity(myIntent);
          }
        });
        listeVin.setOnCreateContextMenuListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wine = new Wine("","","","","");
                Intent myIntent = new Intent(MainActivity.this, VinActivity.class);
                myIntent.putExtra("wine", wine);
                myIntent.putExtra("type", VinActivity.ADD);
                MainActivity.this.startActivity(myIntent);
            }
        });
    }
    public void actualier(){
        wineDB = new WineDbHelper(this);
        Cursor c = wineDB.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter (this, android.R.layout.simple_list_item_2, c, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2});
        listeVin.setAdapter(adapter);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        String action = String.valueOf(item.getTitle());
        if(action.equals("Supprimer")){
            long position = info.id;
            Wine wine = wineDB.findWineById(position);
            if(wineDB.deleteWine(wine)){
                Snackbar.make(info.targetView, wine.getTitle()+" Supprimé !", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                System.out.println(wine.getTitle()+" Supprimé !");
                actualier();
            }
            else{
                System.out.println("Suppression echoué");
                Snackbar.make(info.targetView, "Supppression echoué", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        }
        return super.onContextItemSelected(item);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Supprimer");

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actualiser) {
            actualier();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
