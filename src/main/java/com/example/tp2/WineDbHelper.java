package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";
    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
	 db.execSQL("CREATE TABLE cellar (_id integer primary key autoincrement, name varchar, region varchar, localization varchar, climate varchar, publisher varchar)");
        populate(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(SQLiteDatabase db, Wine wine) throws DuplicateWineException, EmptyWineException {
        boolean closeDb = false;
        if(db==null){
            db = this.getWritableDatabase();
            closeDb = true;
        }
        if(checkDuplication(db, wine)){
            throw new DuplicateWineException();
        }
        if(wine.getTitle().trim().equals("")){
            throw new EmptyWineException();
        }
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME,wine.getTitle().trim());
        cv.put(COLUMN_CLIMATE,wine.getClimate().trim());
        cv.put(COLUMN_LOC,wine.getLocalization().trim());
        cv.put(COLUMN_PLANTED_AREA,wine.getPlantedArea().trim());
        cv.put(COLUMN_WINE_REGION,wine.getRegion().trim());
        long rowID = 0;
        rowID = db.insert(TABLE_NAME,null,cv);
        System.out.println("ROOOOOOOOOOOOOW ID = "+rowID);
        if(closeDb) db.close();
        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public boolean updateWine(Wine wine) throws DuplicateWineException, EmptyWineException {
        SQLiteDatabase db = this.getWritableDatabase();
        if(checkDuplication(db,wine)){
            throw new DuplicateWineException();
        }
        if(wine.getTitle().trim().equals("")) throw new EmptyWineException();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_NAME,wine.getTitle().trim());
        cv.put(COLUMN_CLIMATE,wine.getClimate().trim());
        cv.put(COLUMN_LOC,wine.getLocalization().trim());
        cv.put(COLUMN_PLANTED_AREA,wine.getPlantedArea().trim());
        cv.put(COLUMN_WINE_REGION,wine.getRegion().trim());
	    int res = db.update(TABLE_NAME, cv, "_ID = ?", new String[]{wine.getId()+""});
        db.close();
        return (res > 0);
    }
    public boolean checkDuplication(SQLiteDatabase db, Wine wine){
        boolean closeDb = false;
        if(db==null) {
            db = this.getReadableDatabase();
            closeDb = true;
        }
        String[] tableColumns = new String[] {
                "*",
        };
        String whereClause = COLUMN_NAME+" = ? AND "+COLUMN_WINE_REGION+" = ? AND _ID <> ?";
        String[] whereArgs = new String[] {
                wine.getTitle(),
                wine.getRegion(),
                wine.getId()+""
        };
        Cursor c = db.query(TABLE_NAME, tableColumns, whereClause, whereArgs, null, null, null);
        if(closeDb) db.close();
        return c.getCount() >0;
    }
    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        System.out.println("Fetchinggggggggggggg");
        SQLiteDatabase db = this.getReadableDatabase();

	Cursor cursor;
	 cursor =  db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public boolean deleteWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = db.delete(TABLE_NAME,"_ID=?", new String[]{wine.getId()+""});
        db.close();
        return res>0;
    }
    public Cursor findCursorByWineId(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] tableColumns = new String[] {
                "*",
        };
        String whereClause = "_ID = ?";
        String[] whereArgs = new String[] {
                id+"",
        };
        Cursor c = db.query(TABLE_NAME, tableColumns, whereClause, whereArgs, null, null, null);
        return c;
    }
    public Wine findWineById(long id){
        Cursor c = findCursorByWineId(id);
        System.out.println("Count = "+c.getColumnIndex("name"));
        if(c.getCount()>0)
            c.moveToFirst();
        return WineDbHelper.cursorToWine(c);
    }
     public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
         try {
           addWine(db, new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
            addWine(db, new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
            addWine(db, new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
            addWine(db, new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
            addWine(db, new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
            addWine(db, new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
            addWine(db, new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
            addWine(db, new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
            addWine(db, new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
            addWine(db, new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
            addWine(db, new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));
        }
        catch (DuplicateWineException e){

        }
        catch (EmptyWineException e){

        }
       /* long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        System.out.println(numRows+" Inserteeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeed");
        Log.d(TAG, "nb of rows="+numRows);*/
    }


    public static Wine cursorToWine(Cursor cursor) {
	    Wine wine = new Wine(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_LOC)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA)));
	    wine.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
        return wine;
    }
}
class EmptyWineException extends  Exception {

}
class DuplicateWineException extends  Exception {

}